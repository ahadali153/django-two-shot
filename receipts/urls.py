from django.urls import path
from receipts.views import (
    show_receipt_list,
    create_receipt,
    show_expense_category_list,
    show_account_list,
    create_expense_category,
    create_account,
)

urlpatterns = [
    path("", show_receipt_list, name="home"),
    path("create/", create_receipt, name="create_receipt"),
    path("categories/", show_expense_category_list, name="category_list"),
    path("accounts/", show_account_list, name="account_list"),
    path(
        "categories/create/", create_expense_category, name="create_category"
    ),
    path("accounts/create/", create_account, name="create_account"),
]
